package com.example.demo.repository;

import com.example.demo.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 2017/9/18
 * songchao
 * 说明:
 */
public interface UserJpaRepository extends JpaRepository<User, Long>{

}
