package com.example.demo.controller;

import com.example.demo.domain.User;
import com.example.demo.repository.UserJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 2017/9/14
 * songchao
 * 说明:
 */
@RestController
public class HelloWorldController {
    @Autowired
    UserJpaRepository userJpaRepository;

    @RequestMapping("/hello")
    public String index() {
        List<User> userList = userJpaRepository.findAll();

        return userList.toString();
    }
}
