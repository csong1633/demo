package com.example.demo.java8;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * 2017/9/22
 * songchao
 * 说明:
 */
public class Lambda {

    public static void main(String[] args) {
        Arrays.asList("a", "b", "c").forEach(e -> System.out.print(e));
        List<String> s = Arrays.asList("a", "d", "b");
        s.sort((e1, e2) -> e1.compareTo(e2));
        System.out.println(s);

    }
}
