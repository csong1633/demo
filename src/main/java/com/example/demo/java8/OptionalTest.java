package com.example.demo.java8;

import lombok.Builder;
import lombok.Data;

import java.util.Optional;

/**
 * 2017/9/25
 * songchao
 * 说明:
 */
@Data
@Builder
public class OptionalTest {

    public static void main(String[] args) {
        Optional< String > fullName = Optional.ofNullable(null);
        System.out.println( "Full Name is set? " + fullName.isPresent() );
        System.out.println( "Full Name: " + fullName.orElseGet( () -> "[none]" ) );
        System.out.println( fullName.map( s -> "Hey " + s + "!" ).orElse( "Hey Stranger!" ) );


        Optional< String > firstName = Optional.of( "Tom" );
        System.out.println( "First Name is set? " + firstName.isPresent() );
        System.out.println( "First Name: " + firstName.orElseGet( () -> "[none]" ) );
        System.out.println( firstName.map( s -> "Hey " + s + "!" ).orElse( "Hey Stranger!" ) );
        System.out.println();


        //Test1 t = new Test1();
        //t.setName("sss");

        //Optional<Test1> tt = Optional.of(t);
       // tt.isPresent();
        //System.out.println(tt.get().name);
    }


    @Data
    public static class Test1 {
        public String name;
    }
}
