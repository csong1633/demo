package com.example.demo.java8;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

/**
 * 2017/9/22 songchao 说明:
 */
public class Method {

    public static class Car {
        public static Car create(final Supplier<Car> supplier) {
          return supplier.get();
        }

        public static void collide(final Car car) {
            System.out.println("Collide" + car.toString());
        }

        public void follow(final Car anthor){
            System.out.println("Follow the" + anthor.toString());
        }

        public void repair() {
            System.out.println("Repair" + this.toString());
        }
    }

    public static void main(String[] args) {
        //构造器引用
        final Car car = Car.create(Car::new);
        final List<Car> cars = Arrays.asList(car);

        //静态方法引用
        cars.forEach(Car::collide);

        //类的成员方法的引用
        cars.forEach(Car::repair);

        //类型是某个实例对象的成员方法的引用
        final Car police = Car.create(Car::new);
        cars.forEach(police::follow);
    }

}
