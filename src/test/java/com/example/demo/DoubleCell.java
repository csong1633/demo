package com.example.demo;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import org.w3c.dom.css.RGBColor;

/**
 * 2017/9/26
 * songchao
 * 说明:
 */
public class DoubleCell implements PdfPCellEvent {
    @Override
    public void cellLayout(PdfPCell pdfPCell, Rectangle position, PdfContentByte[] canvases) {
        PdfContentByte canvas = canvases[PdfPTable.LINECANVAS];
        //canvas.setLineDash(5f, 5f);

        //.setLineWidth(1);
        //canvas.setLineCap(2);

        //canvas.setLineDash(3f, 3f);
//        canvas.rectangle(position.getLeft(), position.getBottom(),
//                position.getWidth(), position.getHeight());

        canvas.setColorFill(new BaseColor(124,182,219));
        canvas.rectangle(position.getLeft() + 2, position.getBottom()+2,
                position.getWidth()-4, position.getHeight()-4);
        canvas.setRGBColorFill(124,182,219 );

//        canvas.moveTo(position.getRight(), position.getRight());
//        canvas.lineTo(position.getRight(), position.getRight());
//
//        canvas.moveTo(position.getLeft(), position.getLeft());
//        canvas.lineTo(position.getLeft(), position.getLeft());

//        canvas.lineTo(position.getBottom(), position.getBottom());
//        canvas.lineTo(position.getTop(), position.getTop());


// construct second line (4 user units lower):
        //canvas.moveTo(position.getLeft(), position.getBottom() - 4);
        //canvas.lineTo(position.getRight(), position.getBottom() - 4);

       // canvas.moveTo(position.getLeft(), position.getBottom()-8);
        //canvas.lineTo(position.getRight(), position.getBottom()-8);
        canvas.stroke();
    }
}
