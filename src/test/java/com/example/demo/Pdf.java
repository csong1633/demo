package com.example.demo;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import com.itextpdf.tool.xml.XMLWorkerHelper;


/**
 * 2017/9/25
 * songchao
 * 说明:
 */
public class Pdf {

    public static final String HTML = "E:/b.html";
    public static void main(String[] args) throws Exception {
        File file = new File("E:\\ITextTest.pdf");
        file.getParentFile().mkdirs();
        Document document = new Document();
        // step 2
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
        // step 3
        document.open();
        // step 4
        XMLWorkerHelper.getInstance().parseXHtml(writer, document,
                new FileInputStream(HTML), Charset.forName("UTF-8"));
        // step 5
        document.close();


        /*File file = new File("E:\\ITextTest.pdf");
        file.getParentFile().mkdirs();

        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream("E:\\ITextTest.pdf"));
        document.open();

        XMLWorkerHelper.getInstance().parseXHtml(writer, document,
                new FileInputStream(HTML), Charset.forName("UTF-8"));*/

       /* BaseFont bfChinese = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font titleChinese = new Font(bfChinese, 14, Font.BOLD, new BaseColor(0, 0, 0)); // 模板抬头的字体
        Font itemChinese = new Font(bfChinese, 11, Font.NORMAL, new BaseColor(16,107,150)); // 币种和租金金额的小一号字体
        BaseColor bgColor = new BaseColor(233, 243, 249);
        BaseColor borderColor = new BaseColor(124,182,219);


        Paragraph title = new Paragraph("重点原创网络视听节目规划信息备案表(A1)", titleChinese);// 抬头
        title.setAlignment(Element.ALIGN_CENTER); // 居中设置
        title.setLeading(1f);// 设置行间距//设置上面空白宽度
        document.add(title);

        float[] widths = { 195, 676 };// 设置表格的列宽和列数 默认是4列

        PdfPTable table = new PdfPTable(widths);// 建立一个pdf表格
        table.setSpacingBefore(20f);// 设置表格上面空白宽度
        table.setTotalWidth(898);// 设置表格的宽度
        table.setWidthPercentage(100);// 设置表格宽度为%100
        table.getDefaultCell().setBorderColor(new BaseColor(124,182,219));
//        table.setTableEvent(new BorderEvent());
        // table.getDefaultCell().setBorder(0);//设置表格默认为无边框
//        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
//        table.getDefaultCell().setCellEvent(new DoubleCell());

        String[] tempValue = { "1", "2011-07-07", "2222元", "233元", "2014-12-22", "3000元", "9999元" }; // 租金期次列表
        int rowCount = 1; // 行计数器
        PdfPCell cell = null;
        // ---表头
        cell = new PdfPCell(new Paragraph("节目名称", itemChinese));// 描述
        cell.setFixedHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBackgroundColor(bgColor);
        cell.setBorderWidth(1);
//        cell.setBorder(Rectangle.NO_BORDER);
//        cell.setCellEvent(new DoubleCell());
        cell.setBorderColor(borderColor);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("一眉道长之引月宝灯", itemChinese));// 描述
        cell.setFixedHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(borderColor);
        cell.setBorderWidth(1);
//        cell.setBorder(Rectangle.NO_BORDER);
//        cell.setCellEvent(new DoubleCell());
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("规划备案号", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(borderColor);
        cell.setBorderWidth(1);
        cell.setBackgroundColor(bgColor);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(borderColor);
        cell.setBorderWidth(1);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("节目制作单位", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(new BaseColor(124,182,219));
        cell.setBorderWidth(1);
        cell.setBackgroundColor(bgColor);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("北京嘎纳影业有限公司", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(new BaseColor(124,182,219));
        cell.setBorderWidth(1);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("广播电视节目制作经营许可证", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(borderColor);
        cell.setBorderWidth(1);
        cell.setBackgroundColor(bgColor);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("(京)字第05667号", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(borderColor);
        cell.setBorderWidth(1);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("节目类型", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(borderColor);
        cell.setBorderWidth(1);
        cell.setBackgroundColor(bgColor);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("网络电影（微电影）", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(borderColor);
        cell.setBorderWidth(1);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("题材", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(borderColor);
        cell.setBorderWidth(1);
        cell.setBackgroundColor(bgColor);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("玄幻", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderWidth(1);
        cell.setBorderColor(borderColor);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("年代", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(borderColor);
        cell.setBorderWidth(1);
        cell.setBackgroundColor(bgColor);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("现代", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(borderColor);
        cell.setBorderWidth(1);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("思想内涵", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(borderColor);
        cell.setBorderWidth(1);
        cell.setBackgroundColor(bgColor);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("本片通过梦境以及黑色幽默的表现方式，描述了茅山派弟子石坚与师弟林九的冲突，从二十年前的女子幻象事件到二十年后石坚为了夺取引月宝灯而再次跟师弟林九发生冲突。用鲜明的例子衬托出两位人物的不同性格以及价值观，反映了人性的两面性——善与恶。本影片用石坚与林九做出了鲜明的对比，最终说明人性本善，从而弘扬了林九的大爱，以及仁爱之心，又通过其他小人物对林九的烘托，再一步提升了林九的高尚人品，升华本片的主旨。影片最后通过石坚妻子的幻象来消除石坚心中的怨恨以及不满，表达了该影片作者对世界充满希望以及爱，和对生活的热爱，对一切美好事物的向往。以及想告诉观众本影片的中心主旨是一切黑暗、不公、仇恨、怨恨、丑陋只是暂时的，到最后光明总是战胜黑暗，世界充满希望以及美好，社会是积极向上的，阳光总在风雨后，以及生活的美好。", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(borderColor);
        cell.setBorderWidth(1);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("内容概要", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(borderColor);
        cell.setBorderWidth(1);
        cell.setBackgroundColor(bgColor);
        table.addCell(cell);

        cell = new PdfPCell(new Paragraph("故事讲述民国时期，一九四五年日本战败投降，中国八年抗战胜利，全国各地陆续光伏。然后在这一片升平当中却危机四伏。九叔与师兄石坚在一处深山中遇一女子幻象，因一女子幻象起争执，两人在如何处置女子幻象时，九叔与石坚意见不一，起了争执，在最后石坚欲打算将女子幻象神魂毁灭时，九叔心生不忍将女子幻象救下时不慎打伤石坚，石坚记恨在心，负伤离开，远离中原。 二十年后，九叔与秋生文才在村中开办了一间伏羲堂，以帮助村民驱邪避凶，看风水来营生。但奈何村中因九叔的存在，从而村里一片祥和吉兆，周围一片平静安全。无奈的九叔只有白事作法，算命起名之类的小事来赚取钱银。伏羲堂门可罗雀，师徒三人的日子寒酸窘迫，难以维持生计。 村中新搬迁来的富商王老爷恰巧遇上新宅闹傀儡事件，便在村中悬赏捉傀儡，恰巧被经过的秋生瞧见，秋生对悬赏心动不已，撕下悬赏来到伏羲堂，回到伏羲堂怂恿九叔前往富商王老爷府中前去捉傀儡，赚取平日的花费，九叔无奈下前往王府。师徒三人前往王府后，九叔遇见二十年未见的师兄，石坚。 九叔以为石坚也是前来助王老爷捉傀儡，实际上石坚回到中原的目的是盗取王老爷家中的引月宝灯，让亡妻重返人间。王老爷告知九叔与石坚其实王府并无傀儡，而是自己的女儿王蝶衣神魂不全，痴痴傻傻。欲求助九叔与石坚助其女儿痊愈，众人前往后院见王蝶衣，九叔师徒见着王蝶衣时都大吃一惊。却原来王蝶衣与自己已故的妻子怡红外形极其相似，秋生心中激动不已，欲以怡红相认。却被九叔阻止，九叔解释道王蝶衣不是怡红，是王老爷的女儿。秋生只能冷静下来，不再做纠错。 师兄石坚为了盗取引月宝灯，假情假意之下与九叔合作助王蝶衣恢复其灵魂神智。为了赢取王老爷的信任，石坚吩咐徒弟前往村中水源下蛊毒，随后在王老爷在无药可医的时候站了出来，救了王老爷、王夫人、王蝶衣一家人的生命。王老爷对石坚及其感激，对石坚与九叔开始完全信任，便邀请石坚与九叔两人前往密室一观祖传之物，引月宝灯。百小灵解释银月宝灯只需要点燃宝灯，便可以自我吸纳日月精华！凝聚成灵液，食用一滴，傀儡幻象用了之后修为大进，人类更可延年益寿。 一行人观赏完引月宝灯，文才不相信百小灵说的引月宝灯有那般效果，曾众人离开时自己端详一番，却不慎将引月宝灯点燃。随后引月宝等吸纳日月精华，将沉睡在深山老林中的傀儡唤醒。百小灵预测傀儡会来到村中，百小灵得知傀儡即将到来，告知九叔与石坚等人知晓。就在九叔着手安排如何对付傀儡时，心怀不轨的石坚欲指使徒弟趁乱之时，前往王老爷家中盗取引月宝灯，然而在石坚徒弟盗取引月宝灯的时候，被在王府的文才发现，文才见石坚的徒弟鬼鬼祟祟，便尾随石坚的徒弟，石坚的徒弟在密室盗取宝灯时，被躲在身后的文才发现，将宝灯夺回。然而此时傀儡也来到了村中，石坚也与九叔摊牌，告知九叔自己未曾忘记九叔的那一脚，对石坚而言，是九叔背叛了石坚，身为九叔的大师兄，九叔既然选择救一位恕不相识的女子幻象，心中对其仇恨。 九叔问其为何盗取王老爷的引月宝灯，石坚也不再隐瞒，告知二十年前自从离开了中原，他开始周游各地，遇见了心爱的女人，飞雪。奈何仇家寻上门，飞雪被石坚的仇家给杀害。为了将妻子飞雪复活，石坚多年来一直寻找复活飞雪的办法。如今终于找到引月宝灯可以将飞雪复活的办法，石坚寻找十几年机会，他如何都不会放弃引月宝灯。 九叔为了阻止石坚的，师徒三人齐心将石坚压制下，但傀儡也已经来到王府，九叔等人在对抗傀儡时，又得应付石坚。最后九叔师徒消灭了傀儡，石坚也最终败阵下来，飞雪的灵魂也在这时出现，希望石坚放下过往，重新生活。九叔师徒三人惨胜而归，然而当秋生一夜醒来，发现身上的伤都不见，九叔与文才也完好无损，秋生疑惑不已，这究竟是真实，还是秋生对怡红念念不忘，做的一场梦？", itemChinese));// 描述
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);// 设置内容水平居中显示
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
        cell.setBorderColor(borderColor);
        cell.setBorderWidth(1);
        table.addCell(cell);

        document.add(table);*/

//        Image img = Image.getInstance("E://img.jpg");
//        img.setAlignment(Image.LEFT);
//        img.setBorder(Image.BOX);
//        img.setBorderWidth(10);
//        img.setBorderColor(BaseColor.WHITE);
//        img.scaleToFit(1000, 72);// 大小
//        //img.setRotationDegrees(-30);// 旋转
//        document.add(img);

        document.close();

    }

    public static Font getChineseFont() {
        BaseFont bfChinese;
        Font fontChinese = null;
        try {
            bfChinese = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
            // fontChinese = new Font(bfChinese, 12, Font.NORMAL);
            fontChinese = new Font(bfChinese, 12, Font.NORMAL, BaseColor.BLUE);
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fontChinese;

    }
    /**
     * 获取模板表单，并赋值 固定用法
     *
     * @author ShaoMin
     * @throws Exception
     */
    public void fillFormDatas() throws Exception {

        // 1-封装的数据，这边的key与pdf模板的域名保持一致
        Map<String, String> mMapDatas = new HashMap<String, String>();
        mMapDatas.put("CustomerName", "SAM-SHO");// 客户姓名
        mMapDatas.put("ContNo", "123456789098765");// 合同号
        mMapDatas.put("ContCount", "1");// 保单个数
        mMapDatas.put("EdorType", "CT-退保");// 保全类型
        mMapDatas.put("GetMoney", "999.99");// 保全失算金额
        mMapDatas.put("AcceptName", "人寿保险");// 受理人
        mMapDatas.put("AcceptDate", "2014-11-1");// 受理日期

        // 2-模板和生成的pdf
        Random a = new Random();
        a.nextInt();
        String tPdfTemplateFile = "E://template.pdf";// 获取模板路径
        String tPdfResultFile = "E://" + a.nextInt() + ".pdf";// 生成的文件路径

        // 3-解析PDF模板
        FileOutputStream fos = new FileOutputStream(tPdfResultFile);// 需要生成PDF
        PdfReader reader = new PdfReader(tPdfTemplateFile);// 模板
        PdfStamper mPdfStamper = new PdfStamper(reader, fos);// 解析
        System.out.println(reader.toString());
        // 4-获取到模板上预定义的参数域
        AcroFields form = mPdfStamper.getAcroFields();
        // 获取模板中定义的变量
        Map<String, AcroFields.Item> acroFieldMap = form.getFields();
        // 循环解析模板定义的表单域
        for (Map.Entry<String, AcroFields.Item> entry : acroFieldMap.entrySet()) {
            // 获得块名
            String fieldName = entry.getKey();
            String fieldValue = mMapDatas.get(fieldName);// 通过名字，获取传入的参数值
            if (!"".equals(fieldValue)) {
                // 为模板中的变量赋值(key与pdf模板定义的域名一致)
                form.setField(fieldName, fieldValue);
                System.out.println(fieldName + "," + fieldValue);
            }
        }

        // 模板中的变量赋值之后不能编辑
        mPdfStamper.setFormFlattening(true);
        reader.close();// 阅读器关闭,解析器暂时不关闭，因为创建动态表格还需要使用
        mPdfStamper.close();
    }

    /**
     * 给PDF表格赋值 值动态的，一般建议使用模板， 直接创建绝对位置的表格
     *
     * @author ShaoMin
     * @throws Exception
     */
    public void fillTableDatas() throws Exception {

        // 1-模板和生成的pdf

        String tPdfTemplateFile = "E://template.pdf";// 获取模板路径
        String tPdfResultFile = "E://" + new Random().nextInt() + ".pdf";// 生成的文件路径

        // 2-解析PDF模板
        FileOutputStream fos = new FileOutputStream(tPdfResultFile);// 需要生成PDF
        PdfReader reader = new PdfReader(tPdfTemplateFile);// 模板
        PdfStamper mPdfStamper = new PdfStamper(reader, fos);// 解析

        // 3-获取到模板上预定义的参数域
        AcroFields form = mPdfStamper.getAcroFields();
        // 获取模板中定义的变量
        Map<String, AcroFields.Item> acroFieldMap = form.getFields();
        // 循环解析模板定义的表单域
        int len = 4;
        for (Map.Entry<String, AcroFields.Item> entry : acroFieldMap.entrySet()) {
            // 获得块名
            String fieldName = entry.getKey();
            String fieldValue = "fill_" + len;
            System.out.println(fieldName + ":" + fieldValue);
            form.setField(fieldName, fieldValue);
            len++;
        }

        // 模板中的变量赋值之后不能编辑
        mPdfStamper.setFormFlattening(true);
        reader.close();// 阅读器关闭,解析器暂时不关闭，因为创建动态表格还需要使用
        mPdfStamper.close();

    }
    /**
     * 创建PDF
     *
     * @author ShaoMin
     * @throws Exception
     */

    public void createPDFFile() throws IOException {

        Document document = new Document(PageSize.A4, 80, 79, 20, 45); // A4纸大小,//
        // 左、右、上、下
        try {
            // 中文处理
            BaseFont bfChinese = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);

            Font FontChinese = new Font(bfChinese, 14, Font.NORMAL); // 其他所有文字字体
            Font BoldChinese = new Font(bfChinese, 14, Font.BOLD); // 粗体
            Font titleChinese = new Font(bfChinese, 20, Font.BOLD); // 模板抬头的字体
            Font moneyFontChinese = new Font(bfChinese, 8, Font.NORMAL); // 币种和租金金额的小一号字体
            Font subBoldFontChinese = new Font(bfChinese, 8, Font.BOLD); // 币种和租金金额的小一号字体

            // 使用PDFWriter进行写文件操作
            PdfWriter.getInstance(document, new FileOutputStream("E:\\pdfFile.pdf"));
            document.open(); // 打开文档

            // ------------开始写数据-------------------
            Paragraph title = new Paragraph("起租通知书", titleChinese);// 抬头
            title.setAlignment(Element.ALIGN_CENTER); // 居中设置
            title.setLeading(1f);// 设置行间距//设置上面空白宽度
            document.add(title);

            title = new Paragraph("致：XXX公司", BoldChinese);// 抬头
            title.setSpacingBefore(25f);// 设置上面空白宽度
            document.add(title);

            title = new Paragraph("         贵我双方签署的编号为 XXX有关起租条件已满足，现将租赁合同项下相关租赁要素明示如下：", FontChinese);
            title.setLeading(22f);// 设置行间距
            document.add(title);

            float[] widths = { 10f, 25f, 30f, 30f };// 设置表格的列宽和列数 默认是4列

            PdfPTable table = new PdfPTable(widths);// 建立一个pdf表格
            table.setSpacingBefore(20f);// 设置表格上面空白宽度
            table.setTotalWidth(500);// 设置表格的宽度
            table.setWidthPercentage(100);// 设置表格宽度为%100
            // table.getDefaultCell().setBorder(0);//设置表格默认为无边框

            String[] tempValue = { "1", "2011-07-07", "2222元", "233元", "2014-12-22", "3000元", "9999元" }; // 租金期次列表
            int rowCount = 1; // 行计数器
            PdfPCell cell = null;
            // ---表头
            cell = new PdfPCell(new Paragraph("期次", subBoldFontChinese));// 描述
            cell.setFixedHeight(20);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("租金日", subBoldFontChinese));// 描述
            cell.setFixedHeight(20);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("各期租金金额", subBoldFontChinese));// 描述
            cell.setFixedHeight(20);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
            table.addCell(cell);

            cell = new PdfPCell(new Paragraph("各期租金后\n剩余租金", subBoldFontChinese));// 描述
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
            cell.setFixedHeight(20);
            table.addCell(cell);

            for (int j = 1; j < tempValue.length; j++) {
                if (j % 2 == 1) { // 第一列 日期
                    cell = new PdfPCell(new Paragraph(rowCount + "", moneyFontChinese));// 描述
                    cell.setFixedHeight(20);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
                    table.addCell(cell);
                    rowCount++;
                }
                cell = new PdfPCell(new Paragraph(tempValue[j], moneyFontChinese));// 描述
                cell.setFixedHeight(20);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
                table.addCell(cell);
            }
            document.add(table);

            title = new Paragraph("                租金总额：XXX", FontChinese);
            title.setLeading(22f);// 设置行间距
            document.add(title);
            title = new Paragraph("         特此通知！", FontChinese);
            title.setLeading(22f);// 设置行间距
            document.add(title);
            // -------此处增加图片和日期，因为图片会遇到跨页的问题，图片跨页，图片下方的日期就会脱离图片下方会放到上一页。
            // 所以必须用表格加以固定的技巧来实现
            float[] widthes = { 50f };// 设置表格的列宽和列数
            PdfPTable hiddenTable = new PdfPTable(widthes);// 建立一个pdf表格
            hiddenTable.setSpacingBefore(11f); // 设置表格上空间
            hiddenTable.setTotalWidth(500);// 设置表格的宽度
            hiddenTable.setWidthPercentage(100);// 设置表格宽度为%100
            hiddenTable.getDefaultCell().disableBorderSide(1);
            hiddenTable.getDefaultCell().disableBorderSide(2);
            hiddenTable.getDefaultCell().disableBorderSide(4);
            hiddenTable.getDefaultCell().disableBorderSide(8);

            Image upgif = Image.getInstance("source/imag/bage.png");
            upgif.scalePercent(7.5f);// 设置缩放的百分比%7.5
            upgif.setAlignment(Element.ALIGN_RIGHT);

            cell = new PdfPCell(new Paragraph("", FontChinese));// 描述
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);// 设置内容水平居中显示
            cell.addElement(upgif);
            cell.setPaddingTop(0f); // 设置内容靠上位置
            cell.setPaddingBottom(0f);
            cell.setPaddingRight(20f);
            cell.setBorder(Rectangle.NO_BORDER);// 设置单元格无边框
            hiddenTable.addCell(cell);

            cell = new PdfPCell(new Paragraph("XX 年 XX 月 XX 日                    ", FontChinese));// 金额
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);// 设置内容水平居中显示
            cell.setPaddingTop(0f);
            cell.setPaddingRight(20f);
            cell.setBorder(Rectangle.NO_BORDER);
            hiddenTable.addCell(cell);
            document.add(hiddenTable);
            System.out.println("拼装起租通知书结束...");
            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
